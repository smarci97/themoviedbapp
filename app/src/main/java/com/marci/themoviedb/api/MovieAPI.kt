package com.marci.themoviedb.api

import com.marci.themoviedb.utils.Constants.API_KEY
import com.marci.themoviedb.utils.responses.Token
import com.marci.themoviedb.utils.responses.details.MovieDetailsResponse
import com.marci.themoviedb.utils.responses.movie.MovieResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieAPI {

    @GET("3/search/movie")
    suspend fun searchMovie(
            @Query("api_key") apiKey: String = API_KEY,
            @Query("query") searchQuery: String
            //@Query("language") language : String = "en-US"
    ): Response<MovieResponse> //Call<MovieResponse>

    //https://developers.themoviedb.org/3/authentication/how-do-i-generate-a-session-id

    @GET("3/authentication/token/new")
    suspend fun requestToken(
            @Query("api_key") apiKey: String = API_KEY
    ): Response<Token>

    @GET("3/movie/{movie_id}")
    fun getDetailsOfAMovie(
            @Path("movie_id") movieId: String,
            @Query("api_key") api_key: String = API_KEY
    ): Call<MovieDetailsResponse>
}