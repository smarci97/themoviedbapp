package com.marci.themoviedb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.marci.themoviedb.R
import com.marci.themoviedb.utils.Constants.IMAGE_URL
import com.marci.themoviedb.utils.responses.movie.Movie

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivMovieImage: AppCompatImageView = view.findViewById(R.id.ivMovieImage)
        val tvName: AppCompatTextView = view.findViewById(R.id.tvName)
        val tvBudget: AppCompatTextView = view.findViewById(R.id.tvBudget)
    }

    private val differCallBack = object : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_movie, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = differ.currentList[position]

        holder.itemView.apply {
            Glide.with(this).load("$IMAGE_URL/w500/${item.poster_path}").into(holder.ivMovieImage)
            holder.tvName.text = item.title
        }
        holder.tvBudget.text = if (item.budget == "0" || item.budget == "" || item.budget == null) "No data" else "${item.budget} $"
    }

    override fun getItemCount(): Int = differ.currentList.size
}