package com.marci.themoviedb.utils.responses.details

data class SpokenLanguage(
    val iso_639_1: String,
    val name: String
)