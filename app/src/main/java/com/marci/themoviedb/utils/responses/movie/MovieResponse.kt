package com.marci.themoviedb.utils.responses.movie

import com.marci.themoviedb.utils.responses.movie.Movie

data class MovieResponse(
        val page: Int,
        val results: List<Movie>,
        val total_pages: Int,
        val total_results: Int
)