package com.marci.themoviedb.utils.responses.details

data class Genre(
    val id: Int,
    val name: String
)