package com.marci.themoviedb.utils.responses.details

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)