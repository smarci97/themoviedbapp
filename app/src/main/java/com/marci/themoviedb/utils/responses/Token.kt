package com.marci.themoviedb.utils.responses

data class Token(
    val expires_at: String,
    val request_token: String,
    val success: Boolean
)