package com.marci.themoviedb.ui.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.*
import android.os.Build
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.marci.themoviedb.repository.MovieRepository
import com.marci.themoviedb.utils.Resource
import com.marci.themoviedb.utils.responses.Token
import com.marci.themoviedb.utils.responses.details.MovieDetailsResponse
import com.marci.themoviedb.utils.responses.movie.MovieResponse
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import java.io.IOException


class MainViewModel(
        app: Application,
        private val movieRepository: MovieRepository
) : AndroidViewModel(app) {

    var searchMovies: MutableLiveData<Resource<MovieResponse>> = MutableLiveData()
    var movieDetails: ArrayList<MovieDetailsResponse> = ArrayList()

    var token: MutableLiveData<Resource<Token>> = MutableLiveData()

    fun searchMovies(searchQuery: String) = viewModelScope.launch {
        searchMovies.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = movieRepository.searchMovie(searchQuery)
                searchMovies.postValue(handleSearchMoviesResponse(response))
            } else {
                searchMovies.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> searchMovies.postValue(Resource.Error("Network Failure"))
                else -> searchMovies.postValue(Resource.Error("Conversion Error"))
            }
        }
    }

    private fun handleSearchMoviesResponse(response: Response<MovieResponse>): Resource<MovieResponse> {
        if (response.isSuccessful) {
            response.body()?.let { result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }

    fun getMovieDetails(movieId: String): Call<MovieDetailsResponse> = movieRepository.getMovieDetails(movieId)


    private fun handleMovieDetailsResponse(response: Response<MovieDetailsResponse>): Resource<MovieDetailsResponse> {
        if (response.isSuccessful) {
            response.body()?.let {
                return Resource.Success(it)
            }
        }
        return Resource.Error(response.message())
    }


    fun generateToken() = viewModelScope.launch {
        val response = movieRepository.requestToken()
        token.postValue(handleTokenResponse(response))

    }

    private fun handleTokenResponse(response: Response<Token>): Resource<Token> {
        if (response.isSuccessful) {
            response.body()?.let { result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }

    private fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<Application>().getSystemService(
                Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return true
        } else {
            connectivityManager.activeNetworkInfo?.run {
                return when (type) {
                    TYPE_WIFI -> true
                    TYPE_MOBILE -> true
                    TYPE_ETHERNET -> true
                    else -> false
                }
            }
        }
        return false
    }
}