package com.marci.themoviedb.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.marci.themoviedb.R
import com.marci.themoviedb.adapter.MovieAdapter
import com.marci.themoviedb.repository.MovieRepository
import com.marci.themoviedb.ui.viewmodel.MainViewModel
import com.marci.themoviedb.ui.viewmodel.ViewModelFactory
import com.marci.themoviedb.utils.Resource
import com.marci.themoviedb.utils.responses.details.MovieDetailsResponse
import com.marci.themoviedb.utils.responses.movie.Movie
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MainViewModel
    lateinit var adapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val repo = MovieRepository()
        val factory = ViewModelFactory(application, repo)
        viewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)
        adapter = MovieAdapter()

        generateToken()
        updateUI()
        initSearch()
        setupAdapter()
    }

    fun generateToken() {
        viewModel.token.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    response.data?.let {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.themoviedb.org/authenticate/${it.request_token}"))
                        startActivity(browserIntent, null)
                    }
                }
                is Resource.Error -> {
                    response.data?.let {
                        Toast.makeText(this, "An error occured: $it", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun setupAdapter() {
        viewModel.searchMovies.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data?.let {

                        attachMovieDetails(it.results)

                        adapter.differ.submitList(it.results)
                        if (it.results.isEmpty())
                            error_msg.visibility = View.VISIBLE
                        else
                            error_msg.visibility = View.GONE

                        adapter.notifyDataSetChanged()
                    }
                }
                is Resource.Error -> {
                    hideProgressBar()
                    response.message?.let {
                        Toast.makeText(this, "An error occured: $it", Toast.LENGTH_SHORT).show()
                    }
                }
                is Resource.Loading -> showProgressBar()
            }
        })
    }

    private fun attachMovieDetails(movies: List<Movie>) {

        for (movie in movies) {

            val json = viewModel.getMovieDetails(movie.id.toString())
            json.enqueue(object : Callback<MovieDetailsResponse> {
                override fun onResponse(call: Call<MovieDetailsResponse>, response: Response<MovieDetailsResponse>) {

                    response.body()?.let {
                        movie.budget = response.body()!!.budget
                    }
                    Log.d("RESPONSE", response.body().toString())
                }

                override fun onFailure(call: Call<MovieDetailsResponse>, t: Throwable) {
                    Resource.Error(t.message!!, t)
                }
            })
        }
    }

    private fun initSearch() {
        var job: Job? = null
        etSearch.addTextChangedListener {
            job?.cancel()
            job = MainScope().launch {
                delay(500L)
                it?.let {
                    if (it.toString().isNotEmpty()) {
                        viewModel.searchMovies(it.toString())
                    }
                }
            }
        }
    }

    private fun updateUI() {
        if (rvMovies.adapter == null) {
            rvMovies.adapter = adapter
            rvMovies.layoutManager = LinearLayoutManager(this)
        }
    }

    private fun hideProgressBar() {
        progressbar.visibility = View.GONE
    }

    private fun showProgressBar() {
        progressbar.visibility = View.VISIBLE
    }
}