package com.marci.themoviedb.repository

import com.marci.themoviedb.api.RetrofitInstance

class MovieRepository {

    suspend fun searchMovie(searchQuery: String) =
            RetrofitInstance.api.searchMovie(searchQuery = searchQuery)

    fun getMovieDetails(movieId: String) =
            RetrofitInstance.api.getDetailsOfAMovie(movieId)

    suspend fun requestToken() =
            RetrofitInstance.api.requestToken()
}